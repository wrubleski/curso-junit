package br.com.alura.tdd.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.com.alura.tdd.modelo.Desempenho;
import br.com.alura.tdd.modelo.Funcionario;

public class ReajusteServiceTest {
	private ReajusteService service;
	private Funcionario funcionario;

//	{
//		this.service = new ReajusteService();
//		this.funcionario = new Funcionario("Ana", LocalDate.now(), new BigDecimal("1000.00"));
//	}

	@BeforeEach
	public void inicializar() {
		this.service = new ReajusteService();
		this.funcionario = new Funcionario("Ana", LocalDate.now(), new BigDecimal("1000.00"));
	}

	@Test
	public void reajusteDeveriaSerDeTresPorCentoQuandoDesempenhoForADesejar() {
		this.service.concederReajuste(this.funcionario, Desempenho.A_DESEJAR);
		assertEquals(new BigDecimal("1030.00"), this.funcionario.getSalario());
	}

	@Test
	public void reajusteDeveriaSerDeQuinzePorCentoQuandoDesempenhoForBom() {
		this.service.concederReajuste(this.funcionario, Desempenho.BOM);
		assertEquals(new BigDecimal("1150.00"), this.funcionario.getSalario());
	}

	@Test
	public void reajusteDeveriaSerDeVintePorCentoQuandoDesempenhoForOtimo() {

		this.service.concederReajuste(this.funcionario, Desempenho.OTIMO);
		assertEquals(new BigDecimal("1200.00"), this.funcionario.getSalario());
	}
}
